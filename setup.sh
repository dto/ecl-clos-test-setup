DIR=~/xelftest

mkdir -p $DIR
cd $DIR

echo "Checking out test branches...";

# grab PC branches of 3x0ng and Xelf
git clone git@gitlab.com:dto/3x0ng.git && (cd 3x0ng; git checkout pc;) 
git clone git@gitlab.com:dto/xelf.git && (cd xelf; git checkout pc;) 

# using droid branch of quadrille for this test.
# it's okay, it works with the PC branch of Xelf
git clone git@gitlab.com:dto/quadrille.git && (cd quadrille; git checkout droid;) 

# link these into your local-projects

cd ~/quicklisp/local-projects
ln -s $DIR/quadrille .
ln -s $DIR/xelf .
ln -s $DIR/3x0ng .

echo "Checkout complete.";
echo "------------------";
echo "Make sure to apply the included lispbuilder-video.patch into";
echo "your ~/quicklisp/dists/... before continuing.";



